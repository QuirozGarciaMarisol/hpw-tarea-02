/*Regresa un objeto con las siguientes llaves: "suma", "resta",
  "multiplicacion" y "division" donde sus valores son el resultado de las
  operaciones con dos números como entrada.*/
  function ejercicio06(x,y){
      
    var obj={"suma":(x+y),
             "resta":(x-y),
             "multiplicacion":(x*y),
             "division":(x/y)};
  return obj;
}
  ejercicio06(6, 3);